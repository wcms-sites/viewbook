core = 7.x
api = 2

; uw_viewbook
projects[uw_viewbook][type] = "module"
projects[uw_viewbook][download][type] = "git"
projects[uw_viewbook][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_viewbook.git"
projects[uw_viewbook][download][tag] = "7.x-1.1"
projects[uw_viewbook][subdir] = ""
